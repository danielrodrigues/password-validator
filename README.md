# Password Validator Service

Micro-service designed to validate passwords.

## Dependencies

[Dotnet SDK 5.0.202](https://dotnet.microsoft.com/download/dotnet/5.0)

[Flunt 2.0.3](https://github.com/andrebaltieri/Flunt)

[MediatR 9.0.0](https://github.com/jbogard/MediatR)

[MediatR.Extensions.Microsoft.DependencyInjection 9.0.0](https://github.com/jbogard/MediatR.Extensions.Microsoft.DependencyInjection)

[Microsoft.AspNetCore.Mvc.Versioning 5.0.0](https://github.com/Microsoft/aspnet-api-versioning)

[Microsoft.AspNetCore.Mvc.Versioning.ApiExplorer 5.0.0](https://github.com/Microsoft/aspnet-api-versioning)

[Swashbuckle.AspNetCore 6.1.4](https://github.com/domaindrivendev/Swashbuckle.AspNetCore)

## Build

`$ dotnet build`

## Test

`$ dotnet test`

## Swagger

https://localhost:5001/swagger/

[Swagger doc](https://swagger.io/docs/)

## QuickStart

Usage

```javascript

const http = require('http');

http.get('http://localhost:5000/api/v1/validate?password=AbTp9%21fok', (response) =>{
    let data = '';
    response.on('data', (chunk) => {
        data += chunk;
    });

    response.on('end', () =>{
        console.log(data);
    });
})
.on('error', (error) =>{
    console.log(error);
});

```

## Overview

### What?

PasswordValidator is a micro-service developed in .NET.

### Why?

The micro-service was created mainly to centralize password validation rules.

### Who?

PasswordValidator was developed by Daniel Rodrigues

## Developing

### How to add new validation

Just add the new validation to the password file `PasswordContract.cs`.

> src/PasswordValidator/Domain/ValueObjects/Contracts/PasswordContract.cs

#### Example

```C#
public class PasswordContract : Contract<Password>
{
    public PasswordContract(Password password)
    {
       _password = password;

        Requires()   
            .IsLessOrEqualsThan(_password.Value,
            45,
            "Password",
            $"The password must have a maximum of 45 characters.");
        ...
    }
    ...
}
```

## Detalhamento da solução

A solução poderia ser mais simples, contudo minha intenção foi trazer um conjunto de tecnologias e praticas para deixar mais robusta à solução.

### API

A Api foi criada métodos async e versionamento.
Um segundo endpoint chamado "details" foi criado para validar a senha. Mas a diferença deste endpoint é seu retorno, que no caso de uma senha invalida também retorna a descrição dos erros.

#### Flunt

Uma bom nuget-package para validações em modelos com notificações. Também possui a feature "Contract" que ajudou a agrupar as validações de uma maneira simples.

#### MediatR e Commands

Com a inclusão do mediator e commands, a solução se torna mais flexivel e extensivel. Por exemplo no atual handler de validação poderiamos, injetar um repositorio para incluir uma nova regra, verificar se a senha já foi utilizada anteriormente.

#### XUnit

Havia criado o projeto inicialmente com o "MTest". Porém algumas features do XUnit facilitou os testes de unidade e integração.

#### Swagger

Uma boa solução para documentar Api e agilizar as integrações.

## Melhorias

- Adicionar metricas com prometheus e grafana.
- Integração com SonarQube.
- Adicionar suporte ao protocolo "gRPC".
