using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PasswordValidator.Domain.Commands.Handlers;

namespace PasswordValidator.API.Configurations
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services,
            IConfiguration configuration)
        {
            services
                .AddMediatR(typeof(CheckValidPasswordHandler));

            return services;
        }

    }
}