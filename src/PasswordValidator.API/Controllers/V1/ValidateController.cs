
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PasswordValidator.Domain.Commands.Requests;
using PasswordValidator.Domain.Commands.Responses;

namespace PasswordValidator.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("/v{version:apiVersion}/validate")]

    public class ValidateController : MainController
    {
        private readonly IMediator _mediator;
        public ValidateController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string password = "")
        {
            var result = await GetValidPasswordResponseAsync(password);
            return Ok(result.Valid);
        }

        [HttpGet("details")]
        public async Task<IActionResult> Details(string password = "")
        {
            return Ok(await GetValidPasswordResponseAsync(password));
        }

        private async Task<CheckValidPasswordResponse> GetValidPasswordResponseAsync(string password)
        {
            return await _mediator.Send(new CheckValidPasswordRequest(password));
        }
    }
}