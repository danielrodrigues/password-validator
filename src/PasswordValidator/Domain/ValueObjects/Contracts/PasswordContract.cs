using System.Text.RegularExpressions;
using Flunt.Validations;

namespace PasswordValidator.Domain.ValueObjects.Contracts
{
    public class PasswordContract : Contract<Password>
    {
        private const int MINIMUM_LENGTH = 9;
        private const string SPECIAL_CHARACTERS = "!@#$%^&*()-+";

        private readonly Password _password;

        public PasswordContract(Password password)
        {
            _password = password;

            Requires()
                .IsNotEmpty(_password.Value,
                    "password",
                    "A senha não pode ser vazia.")
                .IsGreaterOrEqualsThan(_password.Value,
                    MINIMUM_LENGTH,
                    "Password",
                    $"A senha deve possuir ao menos {MINIMUM_LENGTH} caracteres.")
                .IsTrue(MustHaveAtLeastOneSpecialCharacterDigit(),
                    "Password",
                    $"A senha deve possuir ao menos um caracter especial ({SPECIAL_CHARACTERS}).")
                .IsTrue(MustHaveAtLeastOneDigit(),
                    "Password",
                    "A senha deve possuir ao menos um digito.")
                .IsTrue(MustHaveLowercase(),
                    "Password",
                    "A senha deve possuir ao menos uma letra minúscula.")
                .IsTrue(MustHaveUppercase(),
                    "Password",
                    "A senha deve possuir ao menos uma letra maiúscula.")
                .IsFalse(MustNotHaveWhiteSpace(),
                    "Password",
                    "A senha não deve possuir espaços em branco.")
                .IsFalse(MustNotHaveRepeatedCharacters(),
                    "Password",
                    "A senha não deve possuir caracteres repetidos dentro do conjunto.");
        }

        private bool MustHaveAtLeastOneSpecialCharacterDigit()
        {
            var regex = new Regex($"(?=.*[{SPECIAL_CHARACTERS}])(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])");
            return regex.IsMatch(_password.Value);
        }
        private bool MustHaveAtLeastOneDigit()
        {
            var regex = new Regex("(?=.*[0-9])");
            return regex.IsMatch(_password.Value);
        }
        private bool MustHaveLowercase()
        {
            var regex = new Regex("(?=.*[a-z])(?=.*[A-Z])");
            return regex.IsMatch(_password.Value);
        }
        private bool MustHaveUppercase()
        {
            var regex = new Regex("(?=.*[A-Z])");
            return regex.IsMatch(_password.Value);
        }
        private bool MustNotHaveWhiteSpace()
        {
            var regex = new Regex("([ ])");
            return regex.IsMatch(_password.Value);
        }
        private bool MustNotHaveRepeatedCharacters()
        {
            var regex = new Regex(@"(\w)*.*\1");
            var teste = regex.IsMatch(_password.Value);
            return teste;
        }
    }
}