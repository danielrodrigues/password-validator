﻿using System.Text.RegularExpressions;
using Flunt.Validations;
using PasswordValidator.Domain.ValueObjects.Contracts;
using PasswordValidator.Shared.ValueObjects;

namespace PasswordValidator.Domain.ValueObjects
{
    public class Password : ValueObject
    {
        public Password(string value)
        {
            Value = value;

            AddNotifications(new PasswordContract(this));
        }
        public string Value { get; private set; }
    }
}
