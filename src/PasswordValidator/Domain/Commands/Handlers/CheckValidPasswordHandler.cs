using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PasswordValidator.Domain.Commands.Requests;
using PasswordValidator.Domain.Commands.Responses;
using PasswordValidator.Domain.ValueObjects;

namespace PasswordValidator.Domain.Commands.Handlers
{
    public class CheckValidPasswordHandler : IRequestHandler<CheckValidPasswordRequest, CheckValidPasswordResponse>
    {
        public Task<CheckValidPasswordResponse> Handle(CheckValidPasswordRequest request, CancellationToken cancellationToken)
        {
            var password = new Password(request.Password);
            var response = new CheckValidPasswordResponse(password.IsValid, password.Notifications);
            return Task.FromResult(response);
        }
    }
}