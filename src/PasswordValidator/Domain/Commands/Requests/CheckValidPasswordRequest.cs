using MediatR;
using PasswordValidator.Domain.Commands.Responses;

namespace PasswordValidator.Domain.Commands.Requests
{
    public class CheckValidPasswordRequest : IRequest<CheckValidPasswordResponse>
    {
        public CheckValidPasswordRequest(string password)
        {
            Password = password;
        }

        public string Password { get; private set; }

    }
}