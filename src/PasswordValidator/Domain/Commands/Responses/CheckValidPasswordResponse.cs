using System.Collections.Generic;
using System.Linq;
using Flunt.Notifications;

namespace PasswordValidator.Domain.Commands.Responses
{
    public class CheckValidPasswordResponse
    {
        private readonly List<Notification> _errors;
        private readonly bool _valid;
        public CheckValidPasswordResponse(bool valid, IReadOnlyCollection<Notification> errors)
        {
            _valid = valid;
            _errors = new List<Notification>();
            _errors.AddRange(errors);
        }

        public bool Valid => _valid;
        public IReadOnlyCollection<string> Errors => _errors.Select(_ => _.Message).ToList();
    }
}