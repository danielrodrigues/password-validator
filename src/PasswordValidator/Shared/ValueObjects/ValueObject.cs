using Flunt.Notifications;

namespace PasswordValidator.Shared.ValueObjects{
    public abstract class ValueObject : Notifiable<Notification>
    {
    }
}