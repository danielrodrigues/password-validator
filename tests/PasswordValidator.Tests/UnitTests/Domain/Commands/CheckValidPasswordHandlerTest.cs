using System.Threading;
using Moq.AutoMock;
using PasswordValidator.Domain.Commands.Handlers;
using PasswordValidator.Domain.Commands.Requests;
using Xunit;

namespace PasswordValidator.Tests.UnitTests.Domain.Commands
{
    public class CheckValidPasswordHandlerTest
    {
        [Fact]
        public void ShouldExecuteWithSuccessValidPasswordReturnTrue()
        {
            //Arrange
            var request = new CheckValidPasswordRequest("AbTp9!fok");

            var mocker = new AutoMocker();
            var handler = mocker.CreateInstance<CheckValidPasswordHandler>();

            //Act
            var result = handler.Handle(request, new CancellationToken());

            //Assert
            Assert.True(result.Result.Valid);
        }

        [Fact]
        public void ShouldExecuteWithSuccessValidPasswordReturnFalse()
        {
            //Arrange
            var request = new CheckValidPasswordRequest("");

            var mocker = new AutoMocker();
            var handler = mocker.CreateInstance<CheckValidPasswordHandler>();

            //Act
            var result = handler.Handle(request, new CancellationToken());

            //Assert
            Assert.False(result.Result.Valid);
        }
    }
}