using PasswordValidator.Domain.ValueObjects;
using PasswordValidator.Domain.ValueObjects.Contracts;
using Xunit;

namespace PasswordValidator.Tests.UnitTests.Domain.ValueObjects.Contracts
{
    public class PasswordContractTest
    {
        [Fact]
        public void ShouldReturnFalseWhenPasswordNotHasUppercase()
        {
            //Arrange
            var password = new Password("asdfghj9$");

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.False(passwordContract.IsValid);
        }

        [Fact]
        public void ShouldReturnFalseWhenPasswordNotHasLowercase()
        {
            //Arrange
            var password = new Password("ASDFGHJ9$");

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.False(passwordContract.IsValid);
        }

        [Fact]
        public void ShouldReturnFalseWhenPasswordNotHasDigit()
        {
            //Arrange
            var password  = new Password("asdfghJw$");

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.False(passwordContract.IsValid);
        }

        [Fact]
        public void ShouldReturnFalseWhenPasswordIsEmpty()
        {
            //Arrange
            var password  = new Password("");

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.False(passwordContract.IsValid);
        }

        [Fact]
        public void ShouldReturnFalseWhenPasswordLengthIsLessThanNine()
        {
            //Arrange
            var password  = new Password("asdfgh$k");

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.False(passwordContract.IsValid);
        }

        [Fact]
        public void ShouldReturnFalseWhenPasswordNotHasSpecialCharacter()
        {
            //Arrange
            var password  = new Password("asdfghjkl");

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.False(passwordContract.IsValid);
        }

        [Fact]
        public void ShouldReturnFalseWhenPasswordHasWhiteSpaces()
        {
            //Arrange
            var password  = new Password("AbT 9!fok");

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.False(passwordContract.IsValid);
        }

        [Fact]
        public void ShouldReturnFalseWhenPasswordHasRepeatedCharacters()
        {
            //Arrange
            var password  = new Password("AbTb9!fok");

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.False(passwordContract.IsValid);
        }

        [Fact]
        public void ShouldReturnFalseWhenPasswordHasRepeatedDigits()
        {
            //Arrange
            var password  = new Password("AbT99!fok");

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.False(passwordContract.IsValid);
        }

        [Fact]
        public void ShouldReturnTrueWhenPasswordIsCreatedCorrectly()
        {
            //Arrange
            var password  = new Password("AbTp9!fok");

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.True(passwordContract.IsValid);
        }

        [Theory]
        [InlineData("")]
        [InlineData("aa")]
        [InlineData("ab")]
        [InlineData("AAAbbbCc")]
        [InlineData("AbTp9!foo")]
        [InlineData("AbTp9!foA")]
        [InlineData("AbTp9 fok")]
        [InlineData("AbTp9!fkk")]
        [InlineData("AbTT9!fok")]
        public void ShouldReturnFalseWhenPasswordFormatIsIncorrect(string value)
        {
            //Arrange
            var password = new Password(value);

            //Act
            var passwordContract = new PasswordContract(password);

            //Assert
            Assert.False(passwordContract.IsValid);
        }
    }
}

