using PasswordValidator.Domain.ValueObjects;
using Xunit;

namespace PasswordValidator.Tests.UnitTests.Domain.ValueObjects
{
    public class PasswordTest
    {
        [Fact]
        public void ShouldReturnFalseWhenPasswordInvalid()
        {
            //Arrange
            var passwordValue = "";

            //Act
            var password = new Password(passwordValue);

            //Assert
            Assert.False(password.IsValid);
            Assert.Equal(6, password.Notifications.Count);
        }

        [Fact]
        public void ShouldReturnNotificationsWhenPasswordInvalid()
        {
            //Arrange
            var passwordValue = "";
            var notificationCount = 6;

            //Act
            var password = new Password(passwordValue);

            //Assert
            Assert.Equal(notificationCount, password.Notifications.Count);
        }

        [Fact]
        public void ShouldReturnTrueWhenPasswordIsValid()
        {
            //Arrange
            var passwordValue  = "AbTp9!fok";

            //Act
            var password = new Password(passwordValue);

            //Assert
            Assert.True(password.IsValid);
        }

        [Fact]
        public void ShouldReturnEmptyNotificationsWhenPasswordIsValid()
        {
            //Arrange
            var passwordValue  = "AbTp9!fok";
            var notificationCount = 0;

            //Act
            var password = new Password(passwordValue);

            //Assert
            Assert.Equal(notificationCount, password.Notifications.Count);
        }
    }
}