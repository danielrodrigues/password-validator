using System;
using System.Text.Json;
using System.Threading.Tasks;
using PasswordValidator.API;
using PasswordValidator.Tests.Config;
using Xunit;

namespace PasswordValidator.Tests.IntegrationTests
{
    [Collection(nameof(IntegrationApiTestsFixtureCollection))]
    public class PasswordTest
    {
        private readonly IntegrationApiTestsFixture<StartupTest> _testsFixture;

        public PasswordTest(IntegrationApiTestsFixture<StartupTest> testsFixture)
        {
            _testsFixture = testsFixture;
        }

        [Fact(DisplayName = "Validate a password in the correct format")]
        public async Task ShouldReturnTrueWhenPasswordCorrectFormat()
        {
            //Arrange
            var passwordValue = "AbTp9%21fok";
            var initialResponse = await _testsFixture.Client.GetAsync("/api/v1/validate");
            initialResponse.EnsureSuccessStatusCode();

            //Act
            var getResponse = await _testsFixture.Client.GetAsync($"/api/v1/validate?password={passwordValue}");
            var isValid = await getResponse.Content.ReadAsStringAsync();

            //Assert
            Assert.True(Convert.ToBoolean(isValid));
        }

        [Fact(DisplayName = "Validate a password in the incorrect format")]
        public async Task ShouldReturnFalseWhenPasswordIncorrectFormat()
        {
            //Arrange
            var passwordValue = "AbTpp9%21fok";
            var initialResponse = await _testsFixture.Client.GetAsync("/api/v1/validate");
            initialResponse.EnsureSuccessStatusCode();

            //Act
            var getResponse = await _testsFixture.Client.GetAsync($"/api/v1/validate?password={passwordValue}");
            var isValid = await getResponse.Content.ReadAsStringAsync();

            //Assert
            Assert.False(Convert.ToBoolean(isValid));
        }

        [Fact(DisplayName = "Validate a password in the correct format")]
        public async Task ShouldReturnDetailsOfValidationWhenPasswordIsCorrectFormat()
        {
            //Arrange
            var passwordValue = "AbTp9%21fok";
            var initialResponse = await _testsFixture.Client.GetAsync("/api/v1/validate/details");
            initialResponse.EnsureSuccessStatusCode();

            //Act
            var getResponse = await _testsFixture.Client.GetAsync($"/api/v1/validate/details?password={passwordValue}");
            var jsonContent = await getResponse.Content.ReadAsStringAsync();
            var content = JsonSerializer.Deserialize<FakeResponseContent>(jsonContent);
            //Assert
            Assert.True(content.valid);
            Assert.Empty(content.errors);
        }

        [Fact(DisplayName = "Validate a password in the incorrect format and return list of errors")]
        public async Task ShouldReturnDetailsOfValidationWhenPasswordIncorrectFormat()
        {
            //Arrange
            var passwordValue = "AbTpp9%21fok";
            var initialResponse = await _testsFixture.Client.GetAsync("/api/v1/validate/details");
            initialResponse.EnsureSuccessStatusCode();

            //Act
            var getResponse = await _testsFixture.Client.GetAsync($"/api/v1/validate/details?password={passwordValue}");
            var jsonContent = await getResponse.Content.ReadAsStringAsync();
            var content = JsonSerializer.Deserialize<FakeResponseContent>(jsonContent);
            //Assert
            Assert.False(content.valid);
            Assert.Single(content.errors);
        }
    }
}