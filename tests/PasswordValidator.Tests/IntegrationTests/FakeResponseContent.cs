using System.Collections.Generic;

namespace PasswordValidator.Tests.IntegrationTests
{
    public class FakeResponseContent
    {
        public FakeResponseContent()
        {
            errors = new List<string>();
        }
        public bool valid { get; set; }
        public List<string> errors { get; set; }
    }
}