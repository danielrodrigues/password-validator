using System;
using System.Net.Http;
using PasswordValidator.API;
using Xunit;

namespace PasswordValidator.Tests.Config
{
    [CollectionDefinition(nameof(IntegrationApiTestsFixtureCollection))]
    public class IntegrationApiTestsFixtureCollection : ICollectionFixture<IntegrationApiTestsFixture<StartupTest>>
    {
    }

    public class IntegrationApiTestsFixture<TStartup> : IDisposable where TStartup : class
    {
        public readonly PasswordValidatorAppFactory<TStartup> Factory;

        public HttpClient Client;

        public IntegrationApiTestsFixture()
        {
            Factory = new PasswordValidatorAppFactory<TStartup>();
            Client = Factory.CreateClient();
        }

        public void Dispose()
        {
            Factory.Dispose();
            Client.Dispose();
        }
    }
}